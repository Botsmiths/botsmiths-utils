# Botsmiths-Utils
The Botsmiths Single Header Utility Library is a collection of utility classes and functions extending WPILib implemented inside a single header file.

## Usage ##
To use this library just copy it into your project and include it like any other header file  
```
#!c++
#include "BotsmithsUtils.h"
```